#define _CRT_SECURE_NO_WARNINGS

//#include<stdio.h>
//int main()
//{
//	printf("hello!\n");
//	return 0;
//}


//#include<stdio.h>
//struct sto
////sto无意义 但是一定要有 可以认为是"名称1"
//{
//	char name[20];
//	int tele;
//	char sex[10];
//	int age;
////这里name tele等都被称为结构体的成员名
////结构体中字符数组的括号不能为空？
//};
//struct sto s = { "huangfeijie",14775593419,"nan",18 };
//
//int main()
//{
//	printf("%s %d %s %d\n", s.name, s.tele, s.sex, s.age);
////"."的用法-->结构体对象.成员名
////打印结构体不能用zu代替d？
//	&s;
//	struct sto* p = &s;
//	printf("%s %d %s %d\n", p->name, p->tele, p->sex, p->age);
////"->"的用法-->指针变量(俗称指针)->成员名
//	printf("%s %d %s %d\n", (*p).name, (*p).tele, (*p).sex, (*p).age);
////"*"操作符的作用是解引用 可以找到名称对应的对象 这里*p指向的是s 但括号的作用不清楚 不用会报错
//	int a = 10;
//	int* m = &a;
//    *m = 20;
////这里因为m已经被定义为整形 所以不用再次对m进行定义 而是直接进行解引用
//	printf("%d\n", a);
////这是一个解引用的例子
//	return 0;
//}
////学的正开心电脑没电了  纪念一下


//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	printf("%d\n", a + b);
//	return 0;
//}
////简单加法函数


//#include<stdio.h>
//int main()
//{
//    int a = 10;
//	printf("%d\n", a);
//	return 0;
//}
////全局变量怎么定义


//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	float a = 0;
//	float b = 0;
//	scanf("%f %f", &a, &b);
//	float c = a + b;
//	printf("%f\n", c);
//	char arr1[] = { "费解爱夏茹" };
//	char arr2[] = { "睡觉啦" };
//	printf("%s\n %s\n", arr1, arr2);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	printf("%s\n", "hello");
//	return 0;
//}


//#include<stdio.h>
//#define STR "hello!"
//int main()
//{
//	printf("%s\n", STR);
//	return 0;
//}


//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = "wake up wake up!";
//	char arr2[] = { 'a','d','f','\0'};
//	//\0操作符可以终止字符的打印
//	printf("%s\n", arr1);
//	printf("%s\n", arr2);
//	printf("%d\n%d\n",strlen(arr1),strlen(arr2));
//	return 0;
//}


//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int a = 0;
//	printf("现在不拼命你拿什么赢？(1/0)\n");
//	scanf("%d", &a);
//	if(a == 1)
//	{
//		printf("学不死就往死里学");
//	}
//	else
//	{
//		printf("学习时间");
//	}
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	printf("为了以后\n");
//	while (a < 5201314)
//	{
//		printf("不死不休！%d\n", a);
//		a++;
//	}
//	if (a = 5201314)
//	{
//		printf("不要放松！%d\n", a);
//	}
//	else
//	{
//		printf("未来可期！\n");
//	}
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	char arr1[] = { 1,2,3,4,5,6,7,8,9,0 };
//	int a = 0;
//	while (a < 10)
//	{
//		printf("%d ", arr1[a]);
//		a++;
//	}
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 7 / 2;
//	printf("%d\n", a);
//	float b = 7 / 2.0;
//	//初始化的时候注意格式float
//	printf("%f\n", b);
//	int c = 7 % 2;
//	//7除以2的余数
//	printf("%d\n", c);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int flag = 0;
//	if (!flag)
//	{
//		printf("%s\n", "hard work!");
//	}
//	if (!!flag)
//	{
//		printf("%s\n", "future");
//	}
//	return 0;
//	//！为反逻辑操作 C语言中0为假 非0为真
//}


//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	float b = 0;
//	double c = 0;
//	printf("%d %d %d", sizeof(a), sizeof(b), sizeof(c));
//	//sizeof为单目操作符 是连着写的
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int arr1[10] = { 0 };
//	printf("%zu\n", sizeof(arr1));
//	printf("%zu\n", sizeof(arr1[0]));
//	//数组中元素的序号从0开始记
//	printf("%zu\n", sizeof(arr1) / sizeof(arr1[0]));
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int b = a++;
//	//先打印再加一
//	printf("%d %d\n", a, b);
//	int c = ++a;
//	//先加一再打印
//	printf("%d\n", c);
//	//--a的操作原理相同
//	int d = (int)520.1314;
//	//强制执行int类型 打印验证
//	printf("%d\n", d);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	//"&&"逻辑上的"并且"
//	//"||"逻辑上的"或者"
//	int a = 10;
//	int b = 20;
//	int c = a > b ? a : b;
//	printf("%d\n", c);
//	int d = a > b ? b : a;
//	printf("%d\n", d);
//	//三目操作符 可以选择出最大/最小值 非常好玩
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 100;
//	int b = 200;
//	int c = 20;
//	int d = (c = b - a, a = b + c, b = a + b);
//	//逗号操作符的运行规则是从左到右 且整个表达式的运行结果为最后一项
//	printf("%d %d %d %d\n", a, b, c, d);
//	printf("%d\n", d);
//	//验证267
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	typedef int HIHI;
//	//typedef关键字可以对类型重命名 这里的HIHI其实就是int类型
//	HIHI a = 10;
//	printf("%d\n", a);
//	return 0;
//}


//#include<stdio.h>
//hard()
//{
//	static int a = 0;
//	//static使得a从局部变量变成了全局变量
//	a++;
//	printf("%d ", a);
//	return 0;
//}
//int main()
//{
//	static int b = 0;
//	while (b < 10)
//	{
//		hard();
//		b++;
//	}
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int* c = &a;
//	printf("%p\n", c);
//	*c = 20;
//	//通过解引用操作符"*" 直接更改a的数值
//	printf("%d\n", a);
//	a = 30;
//	printf("%d\n", a);
//	return 0;
//	//指针变量 地址
//}


//#include<stdio.h>
//int main()
//{
//	struct xiaru
//	{
//		char name[20];
//		int age;
//		char sex[10];
//		int number;
//	};
//	struct  xiaru feijie = {"liuxairu",18,"nv",4477};
//	printf("%s %d %s %d", feijie.name, feijie.age, feijie.sex, feijie.number);
//	int *pfeijie = &feijie;
//	printf("%s %d %s %d",(struct xairu feijie)-> );
//	return 0; 
//}
////报错未解决代码


//#include<stdio.h>
//int main()
//{
//	printf("睡觉睡觉!");
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	while (scanf("%d", &a) != EOF)
//	{
//		if (a >= 140)
//			printf("Genius");
//		else
//			printf("努力大于天赋");
//	}
//	return 0;
//}


//#include <stdio.h>

//int main()
//{
//    int a, b = 0;
//    while (scanf("%d %d", &a, &b) != EOF){
//        while (a > b) {
//            printf("%d>%d", a, b);
//        }
//        if (a < b) {
//            printf("%d<%d", a, b);
//        }
//        else {
//            printf("%d=%d", a, b);
//        }
//    }
//    return 0;
//}


//#include <stdio.h>

//int main() {
//    int a = 0;
//    while (scanf("%d", &a) != EOF) {
//        while (a > 0){
//            printf("*");
//            a--;
//        }
//        printf("\n");
//    }
//    return 0;
//}


//#include<stdio.h>
//int main()
//{
//	for (int a = 3; a <= 100; a += 3)
//	{
//		printf("%d ", a);
//	}
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a, b, c = 0;
//	while (scanf("%d %d %d", &a, &b, &c) != EOF)
//	{
//		if (a >= b) {
//			if (b >= c)
//				printf("%d %d %d", a, b, c);
//			else if (b < c) {
//				if (a <= c)
//					printf("%d %d %d", c, a, b);
//				else if (a > c)
//					printf("%d %d %d", a, c, b);
//			}
//		}
//		else if(a < b) {
//			if (a >= c)
//				printf("%d %d %d", b, a, c);
//			else if (a < c) {
//				if (b >= c)
//					printf("%d %d %d", b, c, a);
//				else if (b < c)
//					printf("%d %d %d", c, b, a);
//			}
//		}
//	}
//	return 0;
//}


//#include <stdio.h>
//int main() {
//    int a, b, c;
//    while (scanf("%d %d %d", &a, &b, &c) != EOF) {
//        if ((a + b) > c && (a + c) > b && (b + c) > a){
//            if (a == b) {
//                if (b == c)
//                    printf("Equilateral triangle!\n");
//                else
//                    printf("Isosceles triangle!\n");
//            }
//            else if (a == c) {
//                if (b == c)
//                    printf("Equilateral triangle!\n");
//                else
//                    printf("Isosceles triangle!\n");
//            }
//            else if (b == c) {
//                if (a == c)
//                    printf("Equilateral triangle!\n");
//                else
//                    printf("Isosceles triangle!\n");
//            }
//            else {
//                printf("Ordinary triangle!");
//            }
//        }
//        else {
//            printf("Not a triangle!\n");
//        }
//    }
//    return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//    double sum1 = 0;
//	double sum2 = 0;
//	for (a = 1; a <= 99; a+=2) {
//		sum1 = sum1 + 1.0 / a;
//	}
//	for (b = 2; b <= 100; b+=2) {
//		sum2 = sum2 + 1.0 / b;
//	}
//	printf("%lf\n", sum1-sum2);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int sum = 0;
//	for (a = 1; a <= 100; ++a) {
//		if (a % 10 == 9)
//			sum++;
//		if (a / 10 == 9)
//			sum++;
//	}
//	printf("%d\n", sum);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	for (int year = 999; year <= 2000; year++) {
//		if ((year % 4 == 0)||(year%100!=0)||(year%400==0))
//			printf("%d  ", year);
//	}
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	int max = 0;
//	for (i = 0; i < 10; i++) {
//		scanf("%d", &arr[i]);//输入十个数据
//	}
//	max = arr[0];
//	for (i = 1; i < 10; i++) {
//		if (arr[i] > max)
//			max = arr[i];
//	}
//	printf("max=%d\n", max);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	do
//	{
//		printf("%d ", a);
//		a++;
//	} while (a <= 10);
//	return 0;
//} 


//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int b = 0;
//	//如果把flag放在这里，那么for循环的flag无法重新定义
//	for (i = 100; i <= 200; i++) {
//		int a = 0;
//		int flag = 0;//这个flag的位置至关重要，关系到第二个if的打印，一定放在里面才能对for的flag进行重定义
//		for (a = 2; a <= i - 1; a++) {
//			if (i % a == 0) {
//				flag = 1;
//				break;
//			}
//		}
//		if (flag == 0) {
//			printf("%d ", i);
//			b++;
//		}
//	}
//	printf("\ncount=%d\n", b);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	char ch1 = 2;
//	char ch2 = 'a';
//	printf("%d %d %c\n", ch1, ch2, ch2);
//	return 0;
//}


//#include<stdio.h>
//#include<time.h>
//#include<stdlib.h>
//void menu()
//{
//	printf("********************\n");
//	printf("****** 1.play ******\n");
//	printf("****** 0.exit ******\n");
//	printf("********************\n");
//}
//void game()
//{
//	int guess = 0;
//	int count = 15;
//	int ret = rand() % 100 + 1;
//	while (1)
//	{
//		printf("你还有%d次机会\n", count);
//		printf("请输入要猜的数字:>");
//		scanf("%d", &guess);
//		if (guess < ret) {
//			printf("猜小了喔，往大点猜呗\n");
//		}
//		else if (guess > ret) {
//			printf("猜大了喔，往小点猜\n");
//		}
//		else
//		{
//			printf("恭喜你，猜对了!,数字是:%d\n", ret);
//			break;
//		}
//		count--;
//		if (count == 0)
//		{
//			printf("动动脑子猜，你没机会咯\n");
//			break;
//		}
//	}
//}
//int main()
//{
//	int input = 0;
//	srand((unsigned int)time(NULL));
//	do {
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("选择错啦，重新选择\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int i, j;
//	for (i = 1; i <= 9; i++)
//	{
//		for (j = 1; j <= i; j++)
//			printf("%2d*%2d=%2d", i, j, i * j);
//		printf("\n");
//	}
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	for (a = 1; a < 10; a++)
//	{
//		int b = 0;
//		for (b = 1; b <= a; b++)
//		{
//			printf("%2d*%2d = %2d  ", a, b, a * b);
//		}
//		printf("\n");
//	}
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	scanf("%d%d", &a, &b);
//	c = (a > b) ? b : a;
//	while ((a % c != 0) || (b % c != 0))
//	{
//		c--;
//	}
//	printf("最大公约数是:%d\n", c);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int count = 0;
//	for (a = 100; a <= 200; a++)
//	{
//		int b = 0;
//		for (b = 2; b <= a; b++)
//		{
//			if (a % b == 0)
//				break;
//		}
//		if (a == b)
//		{
//			printf("%d ", a);
//			count++;
//		}
//	}
//	printf("\n一共有%d个素数\n", count);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	char a = 'a', b = 3;
//	//char a = 'c', b;
//	printf("%d %d\n", a, b);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int arr1[10] = { 0 };
//	int sum = 0;
//	for (int i = 0; i <= 9; i++)
//	{
//		scanf("%d", &arr1[i]);
//		sum += arr1[i];
//	}
//	printf("%lf\n",sum/10.0 );
//	return 0;
//}


//#include <stdio.h>
//
//int main() {
//    int arr[10] = { 0 };
//    for (int a = 0; a < 10; a++)
//    {
//        scanf("%d", &arr[a]);
//    }
//    for (int b = 9; b >= 0; b--)
//    {
//        printf("%d ", arr[b]);
//    }
//    return 0;
//}


//#include <stdio.h>
//int main() {
//    int a, b;
//    while (scanf("%d %d", &a, &b) != EOF) {
//        int arr[2][3];
//        for (int c = 0; c < a; c++)
//        {
//            for (int d = 0; d < b; b++)
//            {
//                scanf("%d", &arr[c][d]);
//            }
//        }
//        for (int d = 0; d < b; d++)
//        {
//            for (int c = 0; c < a; b++)
//            {
//                printf("%d", arr[c][d]);
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}//看看就好,编译不了的嘻嘻


//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int A[] = {1,2,3,4};
//    int B[] = {5,6,7,8};
//	int C = (sizeof(A) / sizeof(A[0]));
//	for (int a = 0; a < C; a++)
//	{
//		printf("%d ", A[a]);
//	}
//	printf("\n");
//	for (int a = 0; a < C; a++)
//	{
//		printf("%d ", B[a]);
//	}
//	printf("\n========\n");
//	int tmp;
//	for(int a = 0; a < C;a++ )
//	{
//		tmp = A[a];
//		A[a] = B[a];
//		B[a] = tmp;
//	}
//	for (int a = 0; a < C; a++)
//	{
//		printf("%d ", A[a]);
//	}
//	printf("\n");
//	for (int a = 0; a < C; a++)
//	{
//		printf("%d ", B[a]);
//	}
//	return 0;
//}


#include<stdio.h>
int the_day_of_year(int a)
{
	if ((a / 4 == 0 && a / 100 != 0) || a / 400 == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
int the_day_of_manth(int a,int b)
{
	int days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	int day = days[b];
	if ((the_day_of_year(a))&&b==2)
	{
		day++;
	}
	return day;
}
int main()
{
	int a, b;
	scanf("%d %d", &a, &b);
	int day = the_day_of_manth(a, b);
	printf("%d", day);
}



